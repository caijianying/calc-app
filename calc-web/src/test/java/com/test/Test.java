package com.test;

import java.util.Arrays;

public class Test {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {}

        ListNode(int val) { this.val = val; }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    /**
     * 主要考察3个知识点，
     * 知识点1：归并排序的整体思想
     * 知识点2：找到一个链表的中间节点的方法
     * 知识点3：合并两个已排好序的链表为一个新的有序链表
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] nums  =new int[]{1,2,3,4,5};
        int[] leftArr =  Arrays.copyOfRange(nums,0,2);
        System.out.println(Arrays.toString(leftArr));
        int[] rightArr = Arrays.copyOfRange(nums,2+1,nums.length);
        System.out.println(Arrays.toString(rightArr));
    }

    @org.junit.Test
    public void test() {
        final ListNode listNode = new ListNode(10);
        listNode.next = new ListNode(3);
        listNode.next.next = new ListNode(8);
        listNode.next.next.next = new ListNode(2);
        listNode.next.next.next.next = new ListNode(5);
        listNode.next.next.next.next.next = new ListNode(9);
        listNode.next.next.next.next.next.next = new ListNode(2);
        listNode.next.next.next.next.next.next.next = new ListNode(8);

        sort(listNode);
    }

    private void sort(ListNode head) {
        ListNode pre = null, p = head, q = head;
        while (q != null && q.next != null) {
            pre = p;
            p = p.next;
            q = q.next.next;
        }

    }
}
