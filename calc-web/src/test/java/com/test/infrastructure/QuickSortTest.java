package com.test.infrastructure;

import org.junit.Test;

import java.util.Arrays;

/**
 * @Author: caijy
 * @Description 快速排序
 * 同冒泡排序一样，快速排序也属于交换排序，通过元素之间的比较和交换位置来达到排序的目的。
 * 快速排序在每一轮挑选一个基准元素，并让其他比它大的元素移动到数列一边，比它小的元素移动到数列的另一边，从而把数列拆解 成两个部分。
 *
 * 有个注意点：
 * 选择基准元素的时候，如果选到数列的最大/最小值，会使原本O(logN)的时间复杂度退化成冒泡排序的最坏时间复杂度O(n^2)
 * @Date: 2021/12/19 星期日 4:00 下午
 */
public class QuickSortTest {

    /**
    * @Author caijy
    * @Description 基准元素默认为第一个元素
    * @Date 2021/12/19 4:03 下午
    **/
    @Test
    public void sortTest(){
        int[] nums = new int[]{49,38,65,97,76,13,27,49};
//        int[] nums = new int[]{4,7,3,5,6,2,8,1};
//        quickSort(nums,0,nums.length-1);
        quickSort2(nums,0,nums.length-1);
        System.out.println(Arrays.toString(nums));
    }

    /**
    * @Author caijy
    * @Description 双边循环法
    * @Date 2021/12/19 10:09 下午
    **/
    public void quickSort(int[] nums,int startIndex,int endIndex){
        if (startIndex >= endIndex){
            return ;
        }
        int pivotIndex = partition(nums,startIndex,endIndex);
        quickSort(nums, startIndex, pivotIndex - 1);
        quickSort(nums, pivotIndex+1, endIndex);
    }

    /**
    * @Author caijy
    * @Description 单边循环法
    * @Date 2021/12/19 10:09 下午
    **/
    public void quickSort2(int[] nums,int startIndex,int endIndex){
        if (startIndex >= endIndex){
            return ;
        }
        int pivotIndex = partition2(nums,startIndex,endIndex);
        quickSort2(nums, startIndex, pivotIndex - 1);
        quickSort2(nums, pivotIndex+1, endIndex);
    }
    private int partition2(int[] nums, int startIndex, int endIndex) {
        int pivot=nums[startIndex];
        int markIndex=startIndex;
        for (int i = startIndex+1; i <= endIndex; i++) {
            if (nums[i] < pivot){
                markIndex ++;
                swap(nums,markIndex,i);
            }
        }
        swap(nums,markIndex,startIndex);
        return markIndex;
    }


    private int partition(int[] nums, int startIndex, int endIndex) {
        int pivot=nums[startIndex];
        int leftIndex=startIndex;
        int rightIndex=endIndex;

        while(leftIndex != rightIndex) {
            while(leftIndex<rightIndex && nums[rightIndex] > pivot){
                rightIndex--;
            }
            while( leftIndex<rightIndex && nums[leftIndex] <= pivot) {
                leftIndex++;
            }
            if(leftIndex<rightIndex) {
                swap(nums,leftIndex,rightIndex);
            }
        }
        swap(nums,leftIndex,startIndex);


        return leftIndex;
    }

    public void swap(int[] nums,int i,int j){
        int tmp=nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }



}
