package com.test.infrastructure;

import org.junit.Test;

import java.util.Arrays;

/**
 * @Author: caijy
 * @Description 计数排序
 * @Date: 2021/12/22 星期三 6:50 上午
 */
public class CountSortTest {

    /**
     * @Author caijy
     * @Description 基本的计数排序
     * 给1到10的随机数排序
     * @Date 2021/12/22 6:51 上午
     **/
    @Test
    public void countSort() {
        int[] nums = new int[]{2, 1, 4, 3, 5, 7, 6, 8, 9, 10, 5, 7, 2};
        //准备一个长度为10的数组
        int[] countArray = new int[10];
        for (int i = 0; i < nums.length; i++) {
            countArray[nums[i] - 1]++;
        }
        for (int i = 0; i < countArray.length; i++) {
            for (int j = 0; j < countArray[i]; j++) {
                System.out.print(i + 1 + " ");
            }
        }
    }

    /**
     * @Author caijy
     * @Description 给90到100的随机数排序
     * 92, 91, 94, 93, 95, 97, 96, 98, 99, 100, 95, 97, 92
     * @Date 2021/12/22 7:00 上午
     **/
    @Test
    public void countSort2() {
        int[] nums = new int[]{92, 91, 94, 93, 95, 97, 96, 98, 99, 100, 95, 97, 92};
        //找到最小值和最大值
        int min = nums[0], max = nums[0];
        for (int num : nums) {
            if (num > max) {
                max = num;
            }
            if (num < min) {
                min = num;
            }
        }
        //基准数为min 计算数组长度
        System.out.println("基准数为" + min);
        int[] countArray = new int[max - min + 1];
        for (int i = 0; i < nums.length; i++) {
            countArray[nums[i] - min]++;
        }

        //打印结果
        for (int i = 0; i < countArray.length; i++) {
            for (int j = 0; j < countArray[i]; j++) {
                System.out.print(i + min + " ");
            }
        }
    }

    /**
     * @Author caijy
     * @Description 假设有个成绩单 给相同成绩的人排名
     * @Date 2021/12/22 7:23 上午
     **/
    @Test
    public void countSort3() {
        int[] nums = new int[]{92, 91, 94, 93, 95, 97, 96, 98, 99, 100, 95, 97, 92};
        //找到最小值和最大值
        int min = nums[0], max = nums[0];
        for (int num : nums) {
            if (num > max) {
                max = num;
            }
            if (num < min) {
                min = num;
            }
        }
        //基准数为min 计算数组长度
        System.out.println("基准数为" + min);
        int[] countArray = new int[max - min + 1];
        for (int i = 0; i < nums.length; i++) {
            countArray[nums[i] - min]++;
        }
        System.out.println(Arrays.toString(countArray));
        //排序数组
        int[] sortArray = new int[countArray.length];
        for (int i = 0; i < sortArray.length; i++) {
            sortArray[nums[i]-min] = getTotal(nums[i]-min,countArray);
        }
        System.out.println(Arrays.toString(sortArray));

        //打印结果
        int[] resultArray = new int[nums.length];
        for (int i = 0; i < resultArray.length; i++) {
            //放到第几个位置
            int resultIndex = sortArray[nums[i]-min] - 1;
            resultArray[resultIndex] = nums[i];
            sortArray[nums[i]-min] --;
        }
        System.out.println(Arrays.toString(resultArray));
    }

    public int getTotal(int index,int[] countArray){
        int total = 0;
        for (int i = 0; i <= index; i++) {
            total += countArray[i];
        }
        return total;
    }
}
