package com.test.infrastructure;

import org.junit.Test;

/**
 * @Author: caijy
 * @Description 排序算法
 * @Date: 2021/12/16 星期四 10:24 下午
 */
public class BubbleSortTest {

    /**
    * @Author caijy
    * @Description 冒泡排序(原始)
    * 思想： 两两比较、交换
    * 时间复杂度 O(n^2)
    * @Date 2021/12/16 10:25 下午
    **/
    @Test
    public void bubbleSort(){
        int compareCount=0;
        int[] nums = new int[]{5,8,6,3,9,2,1,7};
//        int[] nums = new int[]{2,3,4,5,6,7,8,1};
        for (int j = nums.length-1; j > 0 ; j--) {
            for (int i = 0; i < j; i++) {
                if (nums[i+1] < nums[i]){
                    int tmp=nums[i];
                    nums[i]=nums[i+1];
                    nums[i+1]=tmp;
                }
            }
            compareCount ++;
        }
        System.out.println("进行了几轮排序："+compareCount);
        for (int num : nums) {
            System.out.print(num+" ");
        }

//        int compareCount=0;
//        int[] nums = new int[]{2,3,4,5,6,7,8,1};
//        for (int j = 0; j < nums.length-1 ; j++) {
//            for (int i = 0; i < nums.length-1; i++) {
//                if (nums[i+1] < nums[i]){
//                    int tmp=nums[i];
//                    nums[i]=nums[i+1];
//                    nums[i+1]=tmp;
//                }
//            }
//            compareCount ++;
//        }
//        System.out.println("进行了几轮排序："+compareCount);
//        for (int num : nums) {
//            System.out.print(num+" ");
//        }
    }

    /**
    * @Author caijy
    * @Description 改进版
    * 有时候比较几次后已经不需要再做交换，要避免出现多余的循环
    * 比如到第6轮已经排好序了
    * @Date 2021/12/16 10:40 下午
    **/
    @Test
    public void bubbleSort2(){
        int compareCount=0;
        int[] nums = new int[]{5,8,6,3,9,2,1,7};
        for (int j = nums.length-1; j > 0 ; j--) {
            boolean endSort=true;
            for (int i = 0; i < j; i++) {
                if (nums[i+1] < nums[i]){
                    int tmp=nums[i];
                    nums[i]=nums[i+1];
                    nums[i+1]=tmp;
                    endSort = false;
                }
            }
            if (endSort){
                break;
            }
            compareCount ++;
        }
        System.out.println("进行了几轮排序："+compareCount);
        for (int num : nums) {
            System.out.print(num+" ");
        }
    }

    /**
     * @Author caijy
     * @Description 再次改进版
     * 每轮比较，不需要交换位置的元素会被重复比较
     * 换一个数组{3,4,2,1,5,6,7,8}
     * @Date 2021/12/16 10:40 下午
     **/
    @Test
    public void bubbleSort3(){
        int compareCount=0;
//        int[] nums = new int[]{5,8,6,3,9,2,1,7};
//        int[] nums = new int[]{3,4,2,1,5,6,7,8};
        int[] nums = new int[]{2,3,4,5,6,7,8,1};
        int lastIndex=0;
        int sortMarkIndex = nums.length-1;
        for (int j = 0; j < nums.length-1 ; j++) {
            boolean endSort=true;
            int swapCountInner = 0;
            int compareCountInner = 0;
            //原方法
//            for (int i = 0; i < nums.length-1; i++) {
//                compareCountInner ++;
//                if (nums[i+1] < nums[i]){
//                    int tmp=nums[i];
//                    nums[i]=nums[i+1];
//                    nums[i+1]=tmp;
//                    endSort = false;
//                    swapCountInner ++;
//                }
//            }
            //改进方法
            for (int i = 0; i < sortMarkIndex; i++) {
                compareCountInner ++;
                if (nums[i+1] < nums[i]){
                    int tmp=nums[i];
                    nums[i]=nums[i+1];
                    nums[i+1]=tmp;
                    endSort = false;
                    swapCountInner ++;
                    lastIndex = i;
                }
            }
            sortMarkIndex = lastIndex;
            if (endSort){
                break;
            }
            compareCount ++;
            System.out.println("第"+(compareCount)+"轮，比较了"+compareCountInner+"次，交换了"+swapCountInner+"次");
        }
        for (int num : nums) {
            System.out.print(num+" ");
        }
    }


    /**
    * @Author caijy
    * @Description 终极版冒泡排序，即鸡尾酒排序，如同钟摆一样，一轮从左到右一轮从右往左
    * 比如有2,3,4,5,6,7,8,1
    * 除了1其他都已经是有序的 如果冒泡排序从左到右排 还是需要7轮;用鸡尾酒排序，只需要2轮
    * @Date 2021/12/19 2:50 下午
    **/
    @Test
    public void bubbleSort4(){
        int compareCount=0;
        int[] nums = new int[]{2,3,4,5,6,7,8,1};
        for (int j = 0; j < nums.length-1 ; j++) {
            boolean endSort=true;
            if (j%2 == 0){
                for (int i = 0; i < nums.length-1 ; i++) {
                    if (nums[i+1] < nums[i]){
                        int tmp=nums[i];
                        nums[i]=nums[i+1];
                        nums[i+1]=tmp;
                        endSort=false;
                    }
                }
            }
            if (j%2 > 0){
                for (int i = nums.length-1; i >0 ; i--) {
                    if (nums[i] < nums[i-1]){
                        int tmp=nums[i];
                        nums[i]=nums[i-1];
                        nums[i-1]=tmp;
                        endSort=false;
                    }
                }
            }
            if (endSort){
                break;
            }
            compareCount ++;

        }
        System.out.println("进行了几轮排序："+compareCount);
        for (int num : nums) {
            System.out.print(num+" ");
        }
    }

}
