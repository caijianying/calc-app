package com.test.infrastructure;

import org.junit.Test;

import java.util.Arrays;

/**
 * @Author: caijy
 * @Description 堆排序
 * @Date: 2021/12/19 星期日 10:52 下午
 */
public class HeapSortTest {

    @Test
    public void HeapSort() {
        int[] nums = new int[] {4, 7, 6, 9, 8, 3, 2, 1, 5};
//        int[] nums = new int[] {7, 1, 3, 10, 5, 2, 8, 9, 6};
        int len = nums.length;
        for (int parentIndex = (len - 2) / 2; parentIndex >= 0; parentIndex--) {
            buidMaxHeap(nums, parentIndex,len-1);
        }
        System.out.println(Arrays.toString(nums));

        for (int i = nums.length - 1; i >= 0; i--) {
            downAdjust(i, nums);
        }
        System.out.println(Arrays.toString(nums));
    }
    /**
    * @Author caijy
    * @Description 从最后一个叶子节点开始 依次与根结点交换并做下沉调整
    * @Date 2021/12/22 12:42 上午
    **/
    private void downAdjust(int i, int[] nums) {
        int parentIndex = 0;
        //根节点值更大 才会被下沉
        if (nums[parentIndex]>nums[i]){
            swap(nums, parentIndex, i);
            buidMaxHeap(nums,parentIndex,i);
        }
    }

    /**
    * @Author caijy
    * @Description 如果父节点值小于左右孩子的最大值 则与最大值的孩子交换，即父节点的值为最大值
    * @Date 2021/12/22 12:41 上午
    **/
    private void buidMaxHeap(int[] nums, int parentIndex,int lastIndex) {
        int leftIndex = 2 * parentIndex + 1;
        int rightIndex = leftIndex + 1;

        if (leftIndex > lastIndex || rightIndex > lastIndex) {
            return;
        }

        int maxIndex = nums[leftIndex] > nums[rightIndex] ? leftIndex : rightIndex;

        if (lastIndex <= 2 * parentIndex + 2) {//判断左右孩子是否有被删除的元素
            return;
        }

        if (nums[parentIndex] < nums[maxIndex]) {
            swap(nums, parentIndex, maxIndex);
        }

        buidMaxHeap(nums, maxIndex,lastIndex);

    }

    /**
     * @Author caijy
     * @Description 构建最小二叉堆
     * 如果父节点值大于左右孩子的最小值 则与最小值的孩子交换，即父节点的值为最小值
     * @Date 2021/12/20 10:36 下午
     **/
    private void buidMinHeap(int[] nums, int parentIndex) {
        int leftIndex = 2 * parentIndex + 1;
        int rightIndex = leftIndex + 1;

        if (leftIndex > nums.length - 1 || rightIndex > nums.length - 1) {
            return;
        }

        int minIndex = nums[leftIndex] > nums[rightIndex] ? rightIndex : leftIndex;

        if (nums[parentIndex] > nums[minIndex]) {
            swap(nums, parentIndex, minIndex);
        }

        buidMinHeap(nums, leftIndex);
        buidMinHeap(nums, rightIndex);

    }

    public void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}
