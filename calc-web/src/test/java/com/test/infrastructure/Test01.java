package com.test.infrastructure;

import org.junit.Test;

/**
 * @Author: caijy
 * @Description 第一章 时间复杂度和空间复杂度
 * @Date: 2021/12/15 星期三 10:02 下午
 */
public class Test01 {


    /**
    * @Author caijy
    * @Description 时间复杂度的4种情况
     * O(1)<O(logn)<O(n)<O(n的2次方)
    * @Date 2021/12/15 10:48 下午
    **/
    @Test
    public void test01(){
        // O(1)
        System.out.println(">>>>>>");

        //O(logn)
        int n=10000;
        for (int i=n;i>1 ; i/=2) {
            System.out.println(">>>>>>");
        }

        //O(n)
        for (int i = 0; i < n; i++) {
            System.out.println(">>>>>>");
        }

        //O(n的2次方)
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                System.out.println(">>>>>>");
            }
            System.out.println(">>>>>>");
        }
    }

    /**
    * @Author caijy
    * @Description 空间复杂度的3种情况
    * @Date 2021/12/15 10:55 下午
    **/
    @Test
    public void test02(){
        int n=10;
        // 1.O(1)
        int a=0;

        // 2.O(n)
        int[] b = new int[n];
        //递归 的空间复杂度和递归深度成正比
        func(n);

        //3. O(n的2次方) 二维数组
        int[][] c = new int[n][n];


    }

    private void func(int n) {
        if (n<=1){
            return;
        }
        func(n-1);
    }


}
