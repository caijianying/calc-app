package com.test.chapter1_1;

import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.*;

/**
 * @Author: caijy
 * @Description TODO
 * @Date: 2021/12/1 星期三 11:09 下午
 */
public class PracticeTest {


    /**
     * @Author caijy
     * @Description 1.1.1
     * @Date 2021/12/1 11:16 下午
     **/
    @Test
    public void test01() {
//        // 提示：2.0e-6表示科学计数法 为 2乘以10的-6次方
        System.out.println(2.0e-6 * 1000_000_00.1);
//        // && 先执行
        System.out.println(true && false || true && true);
    }

    /**
     * @Author caijy
     * @Description 1.1.3
     * @Date 2021/12/5 9:38 下午
     **/
    @Test
    public void test02() {
        Scanner scanner = new Scanner(System.in);
        while (true){
            List<Integer> list = Lists.newArrayList();
            while (list.size() < 3) {
                System.out.print("输入：");
                list.add(scanner.nextInt());
            }
            int num1 = list.get(0);
            int num2 = list.get(1);
            int num3 = list.get(2);

            boolean eq = num1 == num2 && num2 == num3 && num3 == num1;
            System.out.println(eq ? "equal" : "not equal");
        }
    }

    /**
     * @Author caijy
     * @Description 1.1.5
     * @Date 2021/12/5 10:07 下午
     **/
    @Test
    public void test03() {
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.print("输入X：");
            double x = scanner.nextDouble();
            System.out.print("输入Y：");
            double y = scanner.nextDouble();
            System.out.println(x > 0 && x < 1 && y > 0 && y < 1);
        }
    }
}
