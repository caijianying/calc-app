package com.test.chapter1_1;

import com.google.common.collect.Maps;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * @Author: caijy
 * @Description 一些无关算法的测试
 * @Date: 2021/12/5 星期日 7:26 下午
 */
public class OtherTest {

    /**
    * @Author caijy
    * @Description 模拟考卷 没人题目与答案乱序排列
    * @Date 2021/12/5 7:27 下午
    **/
    @Test
    public void test(){
        String result="B";
        HashMap<String, String> option = Maps.newHashMap();
        option.put("A","1");
        option.put("B","2");
        option.put("C","3");
        option.put("D","以上都不是");
        printSupject(option,result);

        Set<String> keySet = option.keySet();
        List<String> list = Lists.newArrayList(keySet);
        Collections.shuffle(list);
        String newResult="";
        int i=0;
        HashMap<String, String> newOption = Maps.newHashMap();
        for (String key : keySet) {
            String radomKey = list.get(i++);
            if (result.equals(key)){
                newResult = radomKey;
            }
            newOption.put(radomKey,option.get(key));
        }
        printSupject(newOption,newResult);
    }

    /**
     * @Author caijy
     * @Description option 面试题.txt，result答案
     * @Date 2021/12/5 7:01 下午
     **/
    public void printSupject(HashMap<String, String> option,String result){
        String title="1+1=?\n";
        StringBuilder builder = new StringBuilder();
        builder.append(title);
        for (String key : option.keySet()) {
            builder.append(key+". "+option.get(key)+"\n");
        }
        builder.append("答案："+result+"\n");
        System.out.println(builder);
    }

}
