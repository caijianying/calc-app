package com.test.practice;

import org.junit.Test;

import java.util.Stack;

/**
 * @Author: caijy
 * @Description 用栈来模拟一个队列，要求实现队列的两个基本操作:入队、出队。
 * @Date: 2021/12/28 星期二 9:58 下午
 */
public class Practice06 {

    class Queue {
        private int len;
        private Stack<Integer> stack = new Stack();
        Stack<Integer> stack2 = new Stack();

        private void enQueue(int num) {
            stack.push(num);
        }

        private int outQueue() {

            while (stack.size() > 0) {
                stack2.push(stack.pop());
            }
            if (stack2.isEmpty()){
                return -1;
            }
            return stack2.pop();
        }
    }


    @Test
    public void test() {
        Queue queue = new Queue();
        queue.enQueue(10);
        queue.enQueue(9);
        queue.enQueue(22);
        System.out.println(queue.outQueue());
        System.out.println(queue.outQueue());
        queue.enQueue(11);
        System.out.println(queue.outQueue());
        System.out.println(queue.outQueue());
        System.out.println(queue.outQueue());
    }
}
