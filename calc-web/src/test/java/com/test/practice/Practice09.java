package com.test.practice;

import java.util.Arrays;

import org.junit.Test;

/**
 * 给出两个很大的整数，要求实现程序求出两个整数之和.
 */
public class Practice09 {

    /**
     * 此方法待优化
     * 这个方法比较通用 但是当整数特别大的时候 数组长度会很大造成浪费
     * 因此优化点：
     * 将数字分段求和
     * @param bigNum1
     * @param bigNum2
     * @return
     */
    private String sumBigNumber(String bigNum1, String bigNum2) {
        int maxlen = Math.max(bigNum1.length(), bigNum2.length());
        int[] arr1 = new int[maxlen];
        final char[] chars1 = bigNum1.toCharArray();
        for (int i = chars1.length - 1, j = maxlen - 1; i >= 0; i--) {
            arr1[j--] = Integer.valueOf(String.valueOf(chars1[i]));
        }

        int[] arr2 = new int[maxlen];
        final char[] chars2 = bigNum2.toCharArray();
        for (int i = chars2.length - 1, j = maxlen - 1; i >= 0; i--) {
            arr2[j--] = Integer.valueOf(String.valueOf(chars2[i]));
        }

        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));

        int[] result = new int[maxlen + 1];
        int carry = 0;
        for (int i = maxlen,j = maxlen-1; i >= 0 && j>=0; i--,j--) {
            int res = arr1[j] + arr2[j];
            result[i] = res % 10 + carry;
            carry = (res / 10);
        }

        final StringBuilder builder = new StringBuilder();
        if (result[0] == 1){
            builder.append(1);
        }
        for (int i = 1; i < result.length; i++) {
            builder.append(result[i]);
        }

        return builder.toString();
    }

    @Test
    public void test() {
        final String result = sumBigNumber("32545435", "233333343555");
        System.out.println(result);
    }
}
