package com.test.practice;

import org.junit.Test;

/**
 * @Author: caijy
 * @Description 实现一个方法，来判断一个正整数是否是2的整数次幂(如16是2的4次方，返回true;18不是2的整数次幂，则返回false)。
 * 要求性能尽可能高。
 * @Date: 2021/12/28 星期二 8:38 下午
 */
public class Practice04 {

    /**
    * @Author caijy
    * @Description 穷举法 时间复杂度 O(logn)
    * 找出num以内的被2整除的数 一一对比
    * @Date 2021/12/28 8:43 下午
    **/
    private boolean isPowerOf2(int num){
        int tmp = 1;
        while (tmp <= num){
            if (num == tmp){
                return true;
            }
            tmp = tmp << 1;
        }
        return false;
    }

    /**
    * @Author caijy
    * @Description 所有能被2整除的数 最高位都是1 其他位都是0
    * @Date 2021/12/28 8:50 下午
    **/
    private boolean isPowerOf2_1(int num){
        return (num & (num-1)) == 0;
    }




    @Test
    public void test(){
        int num = 512;
        System.out.println(isPowerOf2(num));
        System.out.println(isPowerOf2_1(num));
    }



}
