package com.test.practice;

import org.junit.Test;

import java.util.Stack;

/**
 * @Author: caijy
 * @Description 实现一个栈，该栈带有出栈(pop)、入栈(push)、取最小元素(getMin)3个方法。要保证这3个方法的时间复杂度都是O(1).
 * @Date: 2021/12/27 星期一 8:06 下午
 */
public class Practice02 {
    Stack<Integer> mainStack = new Stack<Integer>();
    Stack<Integer> minStack = new Stack<Integer>();

    private void push(int ele) {
        mainStack.push(ele);
        if (minStack.empty() || ele <= minStack.peek()) {
            minStack.push(ele);
        }
    }

    private int pop() {
        if (mainStack.empty()) {
            return -1;
        }
        Integer pop = mainStack.pop();
        if (!minStack.empty() && minStack.peek() == pop) {
            minStack.pop();
        }
        return pop;
    }

    private int getMin() {
        if (minStack.empty()) {
            return -1;
        }
        return minStack.peek();
    }

    @Test
    public void test() {
        push(3);
        push(1);
        push(2);
        push(4);
        push(5);
        while (true) {
            int pop = pop();
            if (pop == -1) {
                break;
            }
            int min = getMin();
            if (min != -1) {
                System.out.println("出栈：" + pop + " => min:" + min);
            }
        }
    }
}
