package com.test.practice;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.Arrays;

/**
 * @Author: caijy
 * @Description 有一个无序整型数组，如何求出该数组排序后的任意两个相邻元素的最大差值?要求时间和空间复杂度尽可能低.
 * @Date: 2021/12/28 星期二 8:54 下午
 */
public class Practice05 {

    
    /**
    * @Author caijy
    * @Description 这个适合用在差值不是很悬殊的情况，若 1，2，10000 怎么办？可以用桶排序
    * @Date 2021/12/28 9:18 下午
    **/
    private int counter(int[] nums){

        int max=nums[0];
        int min=nums[0];
        for (int num : nums) {
            if (max < num){
                max = num;
            }
            if (min > num){
                min = num;
            }
        }

        int[] numTab = new int[max-min+1];

        for (int i = 0; i < nums.length; i++) {
            numTab[nums[i]-min] ++;
        }
        System.out.println(Arrays.toString(numTab));
        int tmpCount = 1;
        int maxDiff = tmpCount;
        for (int i = 0; i < numTab.length; i++) {
            if (numTab[i] == 0){
                tmpCount ++;
            }else {
                if (maxDiff < tmpCount){
                    maxDiff = tmpCount;
                }
                tmpCount = 1;
            }
        }
        return maxDiff;
    }

    /**
    * @Author caijy
    * @Description TODO 桶排序方法 待完成
    * @Date 2021/12/28 9:56 下午
    **/
    private int counter02(int[] nums){
        int max=nums[0];
        int min=nums[0];
        for (int num : nums) {
            if (max < num){
                max = num;
            }
            if (min > num){
                min = num;
            }
        }
        int d = max-min;
        if (d == 0){
            return 0;
        }
        //计算桶长度
        int len = (max-min)/(nums.length)+1;
        Bucket[] buckets = new Bucket[len];
        for (int i = 0; i < len; i++) {

            buckets[i] = new Bucket();
            buckets[i] = new Bucket();
        }
        //桶排序
        for (int i = 0; i < nums.length; i++) {
            int index = (nums[i] - min)*(len-1)/d;
            if (buckets[index].max < nums[i]){
                buckets[index].max = nums[i];
            }
            if (buckets[index].min > nums[i]){
                buckets[index].min = nums[i];
            }
        }
        System.out.println(JSON.toJSONString(buckets));
        return -1;
    }

    class Bucket{
        private int max;
        private int min;

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }

        public int getMin() {
            return min;
        }

        public void setMin(int min) {
            this.min = min;
        }
    }

    @Test
    public void test(){
        int[] nums = new int[]{10,7,9,3};
        System.out.println(counter(nums));
        System.out.println(counter02(nums));

    }
}
