package com.test.practice;

import org.junit.Test;

/**
 * @Author: caijy
 * @Description 求出两个整数的最大公约数，要尽量优化算法的性能.
 * @Date: 2021/12/27 星期一 8:23 下午
 */
public class Practice03 {

    /**
     * @Author caijy
     * @Description 暴力枚举
     * 如果求10001和10000 性能就很差了
     * @Date 2021/12/27 8:31 下午
     **/
    public int getMax(int num1, int num2) {
        int min = num1 > num2 ? num2 : num1;
        int max = num1 > num2 ? num1 : num2;

        if (max % min == 0) {
            return min;
        }

        for (int i = min / 2; i > 1; i--) {
            if (max % i == 0 && min % i == 0) {
                return i;
            }
        }
        return 1;
    }

    /**
     * @Author caijy
     * @Description 辗转相除法, 又叫作欧几里得算法
     * 这条算法基于一个定理:两个正整数a和b(a>b)，它们的最大公约数等于a除以b的余数c和b之间的最大公约数。
     * 不过，当两个整数较大时，做a%b 取模运算的性能会比较差
     * @Date 2021/12/27 8:45 下午
     **/
    private int getMax2(int num1, int num2) {
        int min = num1 > num2 ? num2 : num1;
        int max = num1 > num2 ? num1 : num2;

        if (max % min == 0) {
            return min;
        }

        return getMax2(max % min, min);
    }

    /**
     * @Author caijy
     * @Description 更相减损术
     * 两个正整数a和b(a>b)，它们的最大公约数等于a-b的差值c和较小数b的最大 公约数。
     * 例如10和25，25减10的差是15，那么10和25的最大公约数，等同于10和15的最大公约数。
     * <p>
     * <p>
     * 但是遇到10000和4，更相减损术运算次数比辗转相除法要多，怎么避免这个问题呢？
     * @Date 2021/12/27 9:37 下午
     **/
    private int getMax3(int num1, int num2) {
        if (num1 == num2) {
            return num1;
        }
        int min = num1 > num2 ? num2 : num1;
        int max = num1 > num2 ? num1 : num2;

        return getMax3(min, max - min);
    }

    /**
     * @Author caijy
     * @Description 位运算做到了2点
     * 1.大整数取模要提高性能
     * 2.减少运算次数
     * 使用位运算判断奇偶数,
     * 1. 两偶
     * 2. 一奇一偶
     * 3. 俩奇数
     * @Date 2021/12/27 10:00 下午
     **/
    private int getMax4(int num1, int num2) {

        if (num1 == num2){
            return num1;
        }

        if ((num1 & 1) == 0 && (num2 & 1) == 0) {
            return getMax4(num1>>1,num2>>1) << 1;
        }else if ((num2 & 1) == 0 && (num2 & 1)!=0) {
            return getMax4(num1>>1,num2);
        }else if ((num2 & 1) != 0 && (num2 & 1)==0){
            return getMax4(num1,num2>>1);
        }else {
            int min = num1 > num2 ? num2 : num1;
            int max = num1 > num2 ? num1 : num2;
            return getMax4(max-min,min);
        }
    }


    @Test
    public void test() {
        System.out.println(getMax(28, 14));
        System.out.println(getMax2(28, 14));
        System.out.println(getMax3(28, 14));
        System.out.println(getMax4(28, 14));
    }
}
