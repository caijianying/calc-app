package com.test.practice;

import java.util.Stack;

import org.junit.Test;
import org.springframework.util.StringUtils;

/**
 * 给出一个整数，从该整数中去掉k个数字，要求剩下的数字形成的新整数尽可能小(保留数字的位置，不能打乱)。
 * 其中整数的长度大于或等于k，给出的整数的大小可以超过long类型的数字范围。
 */
public class Practice08 {

    /**
     * @param number 数字
     * @param k      去掉的数字
     * @return
     */
    private String getMin(String number, int k) {
        if (k <= 0) {
            return number;
        }
        if (k > number.replaceAll(" ", "").length()) {
            throw new RuntimeException("number's count to be removed cannot larger than number's size!");
        }
        final char[] chars = number.toCharArray();
        final Stack<Integer> resultStack = new Stack<>();
        for (char aChar : chars) {
            final String numStr = StringUtils.trimWhitespace(String.valueOf(aChar));
            if (!StringUtils.isEmpty(numStr)) {
                int num = Integer.valueOf(numStr);
                if (k > 0) {
                    if (!resultStack.isEmpty()) {
                        final Integer peek = resultStack.peek();
                        if (peek > num) {
                            resultStack.pop();
                            k--;
                        }
                    }
                }
                resultStack.push(num);
            }
        }

        char[] res = new char[resultStack.size() - k];
        for (int i = res.length - 1; i >= 0; i--) {
            res[i] = resultStack.pop().toString().charAt(0);
        }
        return StringUtils.isEmpty(String.valueOf(res)) ? "0" : String.valueOf(res);
    }

    @Test
    public void test() {
        System.out.println(getMin("623847", 2));
    }
}
