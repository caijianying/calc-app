package com.test.practice;

import org.junit.Test;

import java.util.Arrays;
import java.util.Queue;

/**
 * @Author: caijy
 * @Description 给出一个正整数，找出这个正整数所有数字全排列的下一个数。
 * 说通俗点就是在一个整数所包含数字的全部组合中，找到一个大于且仅大于原数的新整数。
 * 让我们举几个例子。
 * 如果输入12345，则返回12354。 如果输入12354，则返回12435。 如果输入12435，则返回12453。
 * @Date: 2021/12/29 星期三 10:21 下午
 */
public class Practice07 {

    private int getResult(int number) {
        int[] nums = new int[(number + "").length()];

        int i = nums.length - 1;
        while (number / 10 > 0) {
            nums[i] = number % 10;
            number /= 10;
            i--;
        }
        nums[0] = number % 10;

        nums = transform(nums);

        StringBuilder builder = new StringBuilder();
        for (int num : nums) {
            builder.append(num);
        }
        final int result = Integer.parseInt(builder.toString());
        System.out.println(result);
        return result;
    }

    /**
     * @Author caijy
     * @Description 转化 返回一个大于且仅大于原数的新整数
     * @Date 2021/12/29 11:25 下午
     **/
    private int[] transform(int[] nums) {
        int last = nums.length - 1;
        if (nums[last] > nums[last - 1]) {
            swap(nums, last, last - 1);
            return nums;
        }
        exchangeBorder(nums);
        return nums;
    }

    /**
     * 交换边界
     * 1.确认边界下标
     * 2.找到只比边界数大的数（所有比边界数大的数中，它最小），交换位置
     * 3.给边界下标后的数 正序排序
     * @param nums
     * @return
     */
    private void exchangeBorder(int[] nums) {
        int borderIndex = nums.length - 1;
        int j = nums.length - 2;
        int cur = j - 1;
        //是否所有数字都已逆序
        boolean isReverse = true;
        while (cur >= 0) {
            int max = 9;
            for (int k = cur + 1; k < nums.length; k++) {
                if (nums[k] > nums[cur] && nums[k] < max) {
                    max = nums[k];
                    borderIndex = k;
                    isReverse = false;
                }
            }
            if (max < 9) {
                break;
            }
            cur--;
        }
        if (isReverse){
            return;
        }
        swap(nums, borderIndex, cur);
        sortNumber(nums, cur);
    }

    /**
     * 给cur下标后的数 正序排序 这里使用了计数排序
     * @param nums
     * @param cur
     */
    private void sortNumber(int[] nums, int cur) {
        int[] arr = new int[10];
        int startIndex = cur + 1;
        while (startIndex < nums.length) {
            arr[nums[startIndex++]]++;
        }
        startIndex = cur + 1;
        for (int i = 0; i < arr.length && startIndex < nums.length; i++) {
            for (int m = 0; m < arr[i]; m++) {
                nums[startIndex++] = i;
            }
        }
    }

    public void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

    @Test
    public void test() {
        int number = 12345;
        System.out.println(number);
        for (int i = 0; i < 10; i++) {
            number = getResult(number);
        }
    }
}
