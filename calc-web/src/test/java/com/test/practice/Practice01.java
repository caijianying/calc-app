package com.test.practice;

import lombok.Data;
import org.junit.Test;

/**
 * @Author: caijy
 * @Description 如何判断链表有环？若有环，求环长度和入环节点。
 * @Date: 2021/12/23 星期四 11:10 下午
 */
public class Practice01 {

    /**
     * @Author caijy
     * @Description 如何判断链表有环？
     * @Date 2021/12/23 11:35 下午
     **/
    @Test
    public void test01() {
        Node one = new Node(1);
        Node two = new Node(2);
        Node three = new Node(3);
        Node four = new Node(4);
        Node five = new Node(5);

        five.setNext(two);
        four.setNext(five);
        three.setNext(four);
        two.setNext(three);
        one.setNext(two);

        System.out.println(ifCircle(one));
        five.setNext(null);
        four.setNext(five);
        three.setNext(four);
        two.setNext(three);
        one.setNext(two);
        System.out.println(ifCircle(one));
    }

    /**
    * @Author caijy
    * @Description
    * 1.求环长
    * 环长 = 步数差 * node1前进步数
    * @Date 2021/12/23 11:48 下午
    **/
    private boolean ifCircle(Node node) {
        int step = 0;
        int counter = 2;
        Node node1 = node;
        Node node2 = node;
        while (node1 != null && node2 != null && node1.getNext()!=null && node2.getNext()!=null) {
            node1 = node1.getNext();
            node2 = node2.getNext().getNext();
            Object data = node1.getData();
            Object data2 = node2.getData();

            step ++;
            if (data.equals(data2)) {
                counter--;
                if (counter == 1){
                    Node p3 = node;
                    Node p4 = node1;
                    while (!p3.getData().equals(p4.getData())){
                        p3 = p3.getNext();
                        p4 = p4.getNext();
                    }
                    System.out.println("入环节点是：" + p3.getData());
                }
                //判断链表环长
                if (counter == 0) {
                    System.out.println("环长是：" + step);
                    return true;
                }
                step = 0;
            }

        }
        return false;
    }


    @Data
    class Node {
        private Node next;
        private Object data;

        public Node(Object data) {
            this.data = data;
        }
    }
}
