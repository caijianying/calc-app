//给你一个整数数组 coins ，表示不同面额的硬币；以及一个整数 amount ，表示总金额。 
//
// 计算并返回可以凑成总金额所需的 最少的硬币个数 。如果没有任何一种硬币组合能组成总金额，返回 -1 。 
//
// 你可以认为每种硬币的数量是无限的。 
//
// 
//
// 示例 1： 
//
// 
//输入：coins = [1, 2, 5], amount = 11
//输出：3 
//解释：11 = 5 + 5 + 1 
//
// 示例 2： 
//
// 
//输入：coins = [2], amount = 3
//输出：-1 
//
// 示例 3： 
//
// 
//输入：coins = [1], amount = 0
//输出：0
// 
//
// 示例 4： 
//
// 
//输入：coins = [1], amount = 1
//输出：1
// 
//
// 示例 5： 
//
// 
//输入：coins = [1], amount = 2
//输出：2
// 
//
// 
//
// 提示： 
//
// 
// 1 <= coins.length <= 12 
// 1 <= coins[i] <= 2³¹ - 1 
// 0 <= amount <= 10⁴ 
// 
// Related Topics 广度优先搜索 数组 动态规划 👍 1675 👎 0

package leetcode.leetcode.editor.cn;

import java.util.Arrays;

class CoinChange {
    public static void main(String[] args) {
        Solution solution = new CoinChange().new Solution();
        System.out.println(solution.coinChange(new int[]{1,2,5},100));;
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        int[] members;
        public int coinChange(int[] coins, int amount) {
            members = new int[amount+1];
            Arrays.fill(members,999);
            return dp(coins,amount);
        }

        int dp(int[] coins, int amount) {
            if (amount == 0) {return 0;}
            if (amount < 0) {return -1;}
            int res = Integer.MAX_VALUE;
            if (members[amount] != 999){
                return members[amount];
            }

            for (int coin : coins) {
                int result = dp(coins, amount - coin);
                if (result == -1) {
                    continue;
                }
                res = Math.min(res,result+1);
            }

            members[amount] = res == Integer.MAX_VALUE ? -1 : res;
            return members[amount];
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}
