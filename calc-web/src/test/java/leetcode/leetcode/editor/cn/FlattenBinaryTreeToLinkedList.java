//给你二叉树的根结点 root ，请你将它展开为一个单链表： 
//
// 
// 展开后的单链表应该同样使用 TreeNode ，其中 right 子指针指向链表中下一个结点，而左子指针始终为 null 。 
// 展开后的单链表应该与二叉树 先序遍历 顺序相同。 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：root = [1,2,5,3,4,null,6]
//输出：[1,null,2,null,3,null,4,null,5,null,6]
// 
//
// 示例 2： 
//
// 
//输入：root = []
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：root = [0]
//输出：[0]
// 
//
// 
//
// 提示： 
//
// 
// 树中结点数在范围 [0, 2000] 内 
// -100 <= Node.val <= 100 
// 
//
// 
//
// 进阶：你可以使用原地算法（O(1) 额外空间）展开这棵树吗？ 
// Related Topics 栈 树 深度优先搜索 链表 二叉树 👍 1026 👎 0

package leetcode.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

import leetcode.leetcode.editor.cn.common.TreeNode;

class FlattenBinaryTreeToLinkedList {
    public static void main(String[] args) {
        Solution solution = new FlattenBinaryTreeToLinkedList().new Solution();
        final TreeNode n1 = new TreeNode();
        n1.val = 5;
        n1.right = new TreeNode();
        n1.right.val = 6;
        final TreeNode n2 = new TreeNode();
        n2.val = 2;
        n2.left = new TreeNode();
        n2.left.val = 3;
        n2.right = new TreeNode();
        n2.right.val = 4;
        final TreeNode root = new TreeNode();
        root.right = n1;
        root.left = n2;
        root.val = 1;

        solution.flatten(root);
        System.out.println("........");
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        private List<Integer> list = new ArrayList<>();

        public void flatten(TreeNode root) {
            if (root != null) {
                travel(root);
                TreeNode result = root;
                for (int i = 0,len = list.size(); i < len; i++) {
                    int val = list.get(i);
                    result.val = val;
                    if (i != len -1){
                        result.right = new TreeNode();
                        result.left = null;
                        result = result.right;
                    }
                }
            }
        }

        private void travel(TreeNode root) {
            if (root == null) {
                return;
            }
            list.add(root.val);
            if (root.left != null) {
                travel(root.left);
            }
            if (root.right != null) {
                travel(root.right);
            }
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}
