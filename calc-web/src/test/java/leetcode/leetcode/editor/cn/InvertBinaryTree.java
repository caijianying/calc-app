//翻转一棵二叉树。
//
// 示例： 
//
// 输入： 
//
//      4
//   /   \
//  2     7
// / \   / \
//1   3 6   9 
//
// 输出： 
//
//      4
//   /   \
//  7     2
// / \   / \
//9   6 3   1 
//
// 备注: 
//这个问题是受到 Max Howell 的 原问题 启发的 ： 
//
// 谷歌：我们90％的工程师使用您编写的软件(Homebrew)，但是您却无法在面试时在白板上写出翻转二叉树这道题，这太糟糕了。 
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 1121 👎 0

package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.TreeNode;

public class InvertBinaryTree {
    public static void main(String[] args) {
        Solution solution = new InvertBinaryTree().new Solution();
    }

    //leetcode submit region begin(Prohibit modification and deletion)

    class Solution {
        TreeNode result;

        public TreeNode invertTree(TreeNode root) {
            if (root != null) {
                result = new TreeNode();
                TreeNode pre = result;
                travel(root, pre);
                return pre;
            }

            return result;
        }

        private void travel(TreeNode root, TreeNode tmp) {
            if (root == null) {
                return;
            }
            tmp.val = root.val;
            if (root.right != null) {
                tmp.left = new TreeNode();
                travel(root.right, tmp.left);
            }
            if (root.left != null) {
                tmp.right = new TreeNode();
                travel(root.left, tmp.right);
            }

        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}