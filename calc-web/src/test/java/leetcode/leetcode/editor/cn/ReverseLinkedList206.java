package leetcode.leetcode.editor.cn;

import java.util.List;
import java.util.Stack;

import leetcode.leetcode.editor.cn.common.ListNode;

public class ReverseLinkedList206 {
    public static void main(String[] args) {

    }
    //给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
    //
    //
    //
    //
    //
    //
    //
    // 示例 1：
    //
    //
    //输入：head = [1,2,3,4,5]
    //输出：[5,4,3,2,1]
    //
    //
    // 示例 2：
    //
    //
    //输入：head = [1,2]
    //输出：[2,1]
    //
    //
    // 示例 3：
    //
    //
    //输入：head = []
    //输出：[]
    //
    //
    //
    //
    // 提示：
    //
    //
    // 链表中节点的数目范围是 [0, 5000]
    // -5000 <= Node.val <= 5000
    //
    //
    //
    //
    // 进阶：链表可以选用迭代或递归方式完成反转。你能否用两种方法解决这道题？
    //
    // Related Topics递归 | 链表
    //
    // 👍 3174, 👎 0bug 反馈 | 使用指南 | 更多配套插件
    //
    //
    //
    //

    //leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        // 解法：双指针
        public ListNode reverseList(ListNode head) {
            ListNode temp = null;
            while (head != null) {
                ListNode next = head.next;
                head.next = temp;
                temp = head;
                head = next;
            }
            return temp;
        }


    }
    //leetcode submit region end(Prohibit modification and deletion)

}



