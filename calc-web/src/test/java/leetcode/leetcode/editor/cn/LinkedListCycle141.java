package leetcode.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import leetcode.leetcode.editor.cn.common.ListNode;

public class LinkedListCycle141 {

    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    static class Solution {
        private final Set<String> nodes = new HashSet<>();

        public boolean hasCycle(ListNode head) {
            if (head == null) {
                return false;
            }
            if (!nodes.add(head.toString())) {
                return true;
            }
            head = head.next;
            return hasCycle(head);
        }
    }
}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * class ListNode {
 * int val;
 * ListNode next;
 * ListNode(int x) {
 * val = x;
 * next = null;
 * }
 * }
 */

//leetcode submit region end(Prohibit modification and deletion)
