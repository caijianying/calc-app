//给你一个二叉树的根节点 root ，判断其是否是一个有效的二叉搜索树。
//
// 有效 二叉搜索树定义如下： 
//
// 
// 节点的左子树只包含 小于 当前节点的数。 
// 节点的右子树只包含 大于 当前节点的数。 
// 所有左子树和右子树自身必须也是二叉搜索树。 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：root = [2,1,3]
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：root = [5,1,4,null,null,3,6]
//输出：false
//解释：根节点的值是 5 ，但是右子节点的值是 4 。
// 
//
// 
//
// 提示： 
//
// 
// 树中节点数目范围在[1, 10⁴] 内 
// -2³¹ <= Node.val <= 2³¹ - 1 
// 
// Related Topics 树 深度优先搜索 二叉搜索树 二叉树 👍 1384 👎 0

package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.TreeNode;

public class ValidateBinarySearchTree {
    public static void main(String[] args) {
        Solution solution = new ValidateBinarySearchTree().new Solution();
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        private boolean result = true;

        public boolean isValidBST(TreeNode root) {
            return isValid(root);
        }

        private boolean isValid(TreeNode root) {
            if (root == null) {
                return result;
            }
            int rootVal = root.val;

            if (root.left!=null){
                int leftMax = getLeftMax(root.left,root.left.val);
                if (leftMax >= rootVal){
                    result = false;
                    return false;
                }
            }

            if (root.right!=null){
                int rightMin = getRightMin(root.right,root.right.val);
                if (rightMin <= rootVal){
                    result = false;
                    return false;
                }
            }
            return isValid(root.left) && isValid(root.right);
        }

        private int getRightMin(TreeNode rightRoot, int rightMin) {
            if (rightRoot == null){
                return rightMin;
            }
            if (rightRoot.val <= rightMin){
                rightMin = rightRoot.val;
            }
            rightMin = getRightMin(rightRoot.left,rightMin);
            rightMin = getRightMin(rightRoot.right,rightMin);
            return rightMin;
        }

        private int getLeftMax(TreeNode leftRoot,int leftMax) {
            if (leftRoot == null){
                return leftMax;
            }
            if (leftRoot.val >= leftMax){
                leftMax = leftRoot.val;
            }
            leftMax = getLeftMax(leftRoot.left,leftMax);
            leftMax = getLeftMax(leftRoot.right,leftMax);
            return leftMax;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}