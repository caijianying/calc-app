//给定一棵二叉树，返回所有重复的子树。对于同一类的重复子树，你只需要返回其中任意一棵的根结点即可。
//
// 两棵树重复是指它们具有相同的结构以及相同的结点值。 
//
// 示例 1： 
//
//         1
//       / \
//      2   3
//     /   / \
//    4   2   4
//       /
//      4
// 
//
// 下面是两个重复的子树： 
//
//       2
//     /
//    4
// 
//
// 和 
//
//     4
// 
//
// 因此，你需要以列表的形式返回上述重复子树的根结点。 
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 355 👎 0

package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.TreeNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindDuplicateSubtrees {
    public static void main(String[] args) {
        Solution solution = new FindDuplicateSubtrees().new Solution();
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        private Map<String, Integer> resultMap = new HashMap<>();
        private List<TreeNode> resultList = new ArrayList<>();

        public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
            travel(root);
            return resultList;
        }

        private String travel(TreeNode root) {
            if (root == null) {
                return "#";
            }

            String leftVal = travel(root.left);
            String rightVal = travel(root.right);

            String key = root.val + "," + leftVal + "," + rightVal;

            Integer repeactCount = resultMap.getOrDefault(key, 0);
            if (repeactCount == 1) {
                resultList.add(root);
            }
            resultMap.put(key, repeactCount + 1);
            return key;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}