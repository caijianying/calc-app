//返回与给定的前序和后序遍历匹配的任何二叉树。
//
// pre 和 post 遍历中的值是不同的正整数。 
//
// 
//
// 示例： 
//
// 输入：pre = [1,2,4,5,3,6,7], post = [4,5,2,6,7,3,1]
//输出：[1,2,3,4,5,6,7]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= pre.length == post.length <= 30 
// pre[] 和 post[] 都是 1, 2, ..., pre.length 的排列 
// 每个输入保证至少有一个答案。如果有多个答案，可以返回其中一个。 
// 
// Related Topics 树 数组 哈希表 分治 二叉树 👍 210 👎 0

package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.TreeNode;

public class ConstructBinaryTreeFromPreorderAndPostorderTraversal {
    public static void main(String[] args) {
        Solution solution = new ConstructBinaryTreeFromPreorderAndPostorderTraversal().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)

    class Solution {
        private TreeNode node = null;

        public TreeNode constructFromPrePost(int[] preorder, int[] postorder) {
            node = build(preorder, 0, preorder.length - 1, postorder, 0, postorder.length - 1);
            return node;
        }

        private TreeNode build(int[] preorder, int preStart, int preEnd, int[] postorder, int postStart, int postEnd) {
            if (preStart > preEnd) {
                return null;
            }

            if (preStart == preEnd){
                return new TreeNode(preorder[preStart]);
            }

            int rootVal = preorder[preStart];
            TreeNode root = new TreeNode(rootVal);
            int leftRootVal = preorder[preStart + 1];
            int leftRootIndex = 0;
            for (int i = postStart; i <= postEnd; i++) {
                if (postorder[i] == leftRootVal) {
                    leftRootIndex = i;
                    break;
                }
            }
            int leftSize = leftRootIndex - postStart + 1;
            root.left = build(preorder, preStart + 1, preStart + leftSize, postorder, postStart, leftRootIndex);
            root.right = build(preorder, preStart + leftSize + 1, preEnd, postorder, leftRootIndex + 1, postEnd);
            return root;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}