package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.ListNode;

public class MergeTwoSortedLists21 {
    public static void main(String[] args) {

    }
    //将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
    //
    //
    //
    // 示例 1：
    //
    //
    //输入：l1 = [1,2,4], l2 = [1,3,4]
    //输出：[1,1,2,3,4,4]
    //
    //
    // 示例 2：
    //
    //
    //输入：l1 = [], l2 = []
    //输出：[]
    //
    //
    // 示例 3：
    //
    //
    //输入：l1 = [], l2 = [0]
    //输出：[0]
    //
    //
    //
    //
    // 提示：
    //
    //
    // 两个链表的节点数目范围是 [0, 50]
    // -100 <= Node.val <= 100
    // l1 和 l2 均按 非递减顺序 排列
    //
    //
    // Related Topics递归 | 链表
    //
    // 👍 3132, 👎 0bug 反馈 | 使用指南 | 更多配套插件
    //
    //
    //
    //

    //leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
            ListNode listNode = new ListNode(-1, list1);
            ListNode p = listNode, n1 = list1, n2 = list2;

            while (n1 != null && n2 != null) {
                if (n1.val <= n2.val) {
                    p.next = n1;
                    n1 = n1.next;
                } else {
                    p.next = n2;
                    n2 = n2.next;
                }
                p = p.next;
            }

            if (n1 != null) {
                p.next = n1;
            }
            if (n2 != null) {
                p.next = n2;
            }
            return listNode.next;
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}



