package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.ListNode;

public class MiddleOfTheLinkedList876 {
    public static void main(String[] args) {

    }
    //给你单链表的头结点 head ，请你找出并返回链表的中间结点。 
    //
    // 如果有两个中间结点，则返回第二个中间结点。
    //
    //
    //
    // 示例 1：
    //
    //
    //输入：head = [1,2,3,4,5]
    //输出：[3,4,5]
    //解释：链表只有一个中间结点，值为 3 。
    //
    //
    // 示例 2：
    //
    //
    //输入：head = [1,2,3,4,5,6]
    //输出：[4,5,6]
    //解释：该链表有两个中间结点，值分别为 3 和 4 ，返回第二个结点。
    //
    //
    //
    //
    // 提示：
    //
    //
    // 链表的结点数范围是 [1, 100]
    // 1 <= Node.val <= 100
    //
    //
    // Related Topics链表 | 双指针
    //
    // 👍 893, 👎 0bug 反馈 | 使用指南 | 更多配套插件
    //
    //
    //
    //

    //leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        public ListNode middleNode(ListNode head) {
            int len = len(head);
            int midIndex = len / 2;

            int index = 0;
            while (head != null && index < midIndex) {
                head = head.next;
                index++;
            }
            return head;
        }

        private int len(ListNode head) {
            int len = 0;
            while (head != null) {
                head = head.next;
                len++;
            }
            return len;
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}



