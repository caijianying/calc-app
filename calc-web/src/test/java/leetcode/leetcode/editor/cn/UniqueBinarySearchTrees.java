//给你一个整数 n ，求恰由 n 个节点组成且节点值从 1 到 n 互不相同的 二叉搜索树 有多少种？返回满足题意的二叉搜索树的种数。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 3
//输出：5
// 
//
// 示例 2： 
//
// 
//输入：n = 1
//输出：1
// 
//
// 
//
// 提示： 
//
// 
// 1 <= n <= 19 
// 
// Related Topics 树 二叉搜索树 数学 动态规划 二叉树 👍 1507 👎 0

package leetcode.leetcode.editor.cn;

class UniqueBinarySearchTrees {
    public static void main(String[] args) {
        Solution solution = new UniqueBinarySearchTrees().new Solution();
        solution.numTrees(3);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        int[][] remembers;

        public int numTrees(int n) {
            remembers = new int[n + 1][n + 1];
            return countNodes(1, n);
        }

        private int countNodes(int l, int h) {
            if (l > h) {
                return 1;
            }
            if (remembers[l][h] != 0) {
                return remembers[l][h];
            }

            int res = 0;
            for (int mid = l; mid <= h; mid++) {
                int left = countNodes(l, mid - 1);
                int right = countNodes(mid + 1, h);
                res += left * right;
            }
            remembers[l][h] = res;
            return res;
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}
