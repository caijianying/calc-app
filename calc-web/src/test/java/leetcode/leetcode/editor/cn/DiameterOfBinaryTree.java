//给定一棵二叉树，你需要计算它的直径长度。一棵二叉树的直径长度是任意两个结点路径长度中的最大值。
// 这条路径可能穿过也可能不穿过根结点。
//
//
//
// 示例 :
//给定二叉树
//
//           1
//         / \
//        2   3
//       / \
//      4   5
//
//
// 返回 3, 它的长度是路径 [4,2,1,3] 或者 [5,2,1,3]。
//
//
//
// 注意：两结点之间的路径长度是以它们之间边的数目表示。
// Related Topics 树 深度优先搜索 二叉树 👍 870 👎 0

package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.TreeNode;

public class DiameterOfBinaryTree {
    public static void main(String[] args) {
        Solution solution = new DiameterOfBinaryTree().new Solution();
    }


    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int diameterOfBinaryTree(TreeNode root) {
            if (root == null) {
                return 0;
            }
            travel(root);
            return maxPath;
        }

        public int maxPath = 0;

        private int travel(TreeNode node) {
            if (node == null) {
                return maxPath;
            }
            int leftDepth = getDepth(node.left);
            int rightDepth = getDepth(node.right);
            maxPath = Math.max(maxPath, leftDepth + rightDepth);
            travel(node.left);
            travel(node.right);
            return maxPath;
        }

        /**
         * @Author caijy
         * @Description
         * @Date 2022/1/4 11:41 下午
         **/
        private int getDepth(TreeNode head) {
            if (head == null) {
                return 0;
            }
            int leftDepth = this.getDepth(head.left);
            int rightDepth = this.getDepth(head.right);
            return Math.max(leftDepth, rightDepth) + 1;
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}