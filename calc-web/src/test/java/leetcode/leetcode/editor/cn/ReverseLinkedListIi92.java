package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.ListNode;

public class ReverseLinkedListIi92 {
    public static void main(String[] args) {

    }
    //给你单链表的头指针 head 和两个整数 left 和 right ，其中 left <= right 。请你反转从位置 left 到位置 right 的链
    //表节点，返回 反转后的链表 。
    //
    //
    //
    // 示例 1：
    //
    //
    //输入：head = [1,2,3,4,5], left = 2, right = 4
    //输出：[1,4,3,2,5]
    //
    //
    // 示例 2：
    //
    //
    //输入：head = [5], left = 1, right = 1
    //输出：[5]
    //
    //
    //
    //
    // 提示：
    //
    //
    // 链表中节点数目为 n
    // 1 <= n <= 500
    // -500 <= Node.val <= 500
    // 1 <= left <= right <= n
    //
    //
    //
    //
    // 进阶： 你可以使用一趟扫描完成反转吗？
    //
    // Related Topics链表
    //
    // 👍 1575, 👎 0bug 反馈 | 使用指南 | 更多配套插件
    //
    //
    //
    //

    //leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        //public ListNode reverseBetween(ListNode head, int left, int right) {
        //    ListNode pre = null;
        //    ListNode cur = head;
        //    ListNode r = new ListNode(-1);
        //    ListNode res = r;
        //    boolean reverse = false;
        //    int index = 0;
        //    while (cur != null) {
        //        ListNode tmp = cur.next;
        //        if (index == left - 1) {
        //            reverse = true;
        //        }
        //        if (reverse) {
        //            cur.next = pre;
        //            pre = cur;
        //        } else {
        //            res.next = cur;
        //            res = res.next;
        //        }
        //
        //        if (index == right - 1) {
        //            reverse = false;
        //            res.next = pre;
        //            while (res.next != null) {
        //                res = res.next;
        //            }
        //        }
        //        cur = tmp;
        //        index++;
        //    }
        //    return r.next;
        //}

            public ListNode reverseBetween(ListNode head, int m, int n) {
                // 定义一个dummyHead, 方便处理
                ListNode dummyHead = new ListNode(0);
                dummyHead.next = head;

                // 初始化指针
                ListNode g = dummyHead;
                ListNode p = dummyHead.next;

                // 将指针移到相应的位置
                for(int step = 0; step < m - 1; step++) {
                    g = g.next; p = p.next;
                }

                // 头插法插入节点
                for (int i = 0; i < n - m; i++) {
                    ListNode removed = p.next;
                    p.next = p.next.next;

                    removed.next = g.next;
                    g.next = removed;
                }

                return dummyHead.next;
            }

    }
    //leetcode submit region end(Prohibit modification and deletion)

}



