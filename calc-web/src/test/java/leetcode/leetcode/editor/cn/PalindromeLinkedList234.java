package leetcode.leetcode.editor.cn;

import java.util.List;

import leetcode.leetcode.editor.cn.common.ListNode;

public class PalindromeLinkedList234 {
    public static void main(String[] args) {

    }
    //给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。 
    //
    //
    //
    // 示例 1：
    //
    //
    //输入：head = [1,2,2,1]
    //输出：true
    //
    //
    // 示例 2：
    //
    //
    //输入：head = [1,2]
    //输出：false
    //
    //
    //
    //
    // 提示：
    //
    //
    // 链表中节点数目在范围[1, 10⁵] 内
    // 0 <= Node.val <= 9
    //
    //
    //
    //
    // 进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？
    //
    // Related Topics栈 | 递归 | 链表 | 双指针
    //
    // 👍 1705, 👎 0bug 反馈 | 使用指南 | 更多配套插件
    //
    //
    //
    //

    //leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        // 快慢指针 + 翻转
        public boolean isPalindrome(ListNode head) {
            ListNode slow = head;
            ListNode fast = head;
            while (fast != null && fast.next != null) {
                slow = slow.next;
                fast = fast.next.next;
            }
            // 定位中间节点
            if (fast != null) {
                slow = slow.next;
            }

            // 翻转
            //ListNode reverse = this.temp(slow);
            ListNode reverse = this.insertHead2(slow);
            //ListNode reverse = this.insertHead(slow);
            ListNode cur = head;
            while (reverse != null) {
                if (cur.val != reverse.val) {
                    return false;
                }
                cur = cur.next;
                reverse = reverse.next;
            }

            return true;
        }

        /**
         * 头插法
         **/
        private ListNode insertHead(ListNode head) {
            ListNode dump = new ListNode(0, head);
            ListNode cur = head;
            int len = 0;
            while (cur != null) {
                len++;
                cur = cur.next;
            }

            // 初始化指针
            ListNode g = dump;
            ListNode p = dump.next;

            for (int i = 0; i < len - 1; i++) {
                ListNode removed = p.next;
                p.next = p.next.next;
                removed.next = g.next;
                g.next = removed;
            }

            return dump.next;
        }

        /**
         * 头插法
         **/
        private ListNode insertHead2(ListNode head) {
            ListNode dump = new ListNode(0, head);

            // 初始化指针
            ListNode g = dump;
            ListNode p = dump.next;

            while (p != null && p.next != null) {
                ListNode removed = p.next;
                p.next = p.next.next;
                removed.next = g.next;
                g.next = removed;
            }

            return dump.next;
        }

        /**
         * 临时节点法
         **/
        public ListNode temp(ListNode head) {
            ListNode temp = null;
            while (head != null) {
                ListNode next = head.next;
                head.next = temp;
                temp = head;
                head = next;
            }
            return temp;
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}



