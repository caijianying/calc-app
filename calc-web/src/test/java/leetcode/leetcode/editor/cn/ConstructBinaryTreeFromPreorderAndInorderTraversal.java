//给定一棵树的前序遍历 preorder 与中序遍历 inorder。请构造二叉树并返回其根节点。 
//
// 
//
// 示例 1: 
//
// 
//Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
//Output: [3,9,20,null,null,15,7]
// 
//
// 示例 2: 
//
// 
//Input: preorder = [-1], inorder = [-1]
//Output: [-1]
// 
//
// 
//
// 提示: 
//
// 
// 1 <= preorder.length <= 3000 
// inorder.length == preorder.length 
// -3000 <= preorder[i], inorder[i] <= 3000 
// preorder 和 inorder 均无重复元素 
// inorder 均出现在 preorder 
// preorder 保证为二叉树的前序遍历序列 
// inorder 保证为二叉树的中序遍历序列 
// 
// Related Topics 树 数组 哈希表 分治 二叉树 👍 1365 👎 0

package leetcode.leetcode.editor.cn;

import leetcode.leetcode.editor.cn.common.TreeNode;

class ConstructBinaryTreeFromPreorderAndInorderTraversal {
    public static void main(String[] args) {
        Solution solution = new ConstructBinaryTreeFromPreorderAndInorderTraversal().new Solution();
        solution.buildTree(new int[] {3, 9, 20, 15, 7}, new int[] {9, 3, 15, 20, 7});
        System.out.println("........");
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        private int curIndex = 0;

        public TreeNode buildTree(int[] preorder, int[] inorder) {
            return build(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
        }

        /**
         * 先通过前序遍历 得到根节点的值，再根据值去中序遍历找根节点的位置
         */
        private TreeNode build(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart, int inEnd) {
            if (preStart > preEnd) {
                return null;
            }
            int rootVal = preorder[preStart];
            Integer rootIndexIn = null;
            for (int i = inStart; i <= inEnd; i++) {
                if (inorder[i] == rootVal) {
                    rootIndexIn = i;
                    break;
                }
            }
            TreeNode node = new TreeNode(rootVal);
            int leftSize = rootIndexIn - inStart;
            node.left = build(preorder, preStart + 1, preStart + leftSize, inorder, inStart, rootIndexIn - 1);
            node.right = build(preorder, preStart + leftSize + 1, preEnd, inorder, rootIndexIn + 1, inEnd);

            return node;
        }
    }
    //leetcode submit region end(Prohibit modification and deletion)

}
