package leetcode.leetcode.editor.cn.common;

import com.test.Test;
import lombok.Data;

/**
 * @author liguang
 * @date 2023/6/6 星期二 3:00 下午
 */
@Data
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {}

    public ListNode(int val) { this.val = val; }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
