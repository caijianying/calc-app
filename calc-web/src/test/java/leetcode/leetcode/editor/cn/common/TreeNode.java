package leetcode.leetcode.editor.cn.common;


/**
 * @Author: caijy
 * @Description 通用类
 * @Date: 2022/1/5 星期三 11:25 下午
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
