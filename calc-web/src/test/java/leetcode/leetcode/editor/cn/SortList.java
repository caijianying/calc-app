//给你链表的头结点 head ，请将其按 升序 排列并返回 排序后的链表 。 
//
// 进阶： 
//
// 
// 你可以在 O(n log n) 时间复杂度和常数级空间复杂度下，对链表进行排序吗？ 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：head = [4,2,1,3]
//输出：[1,2,3,4]
// 
//
// 示例 2： 
//
// 
//输入：head = [-1,5,3,4,0]
//输出：[-1,0,3,4,5]
// 
//
// 示例 3： 
//
// 
//输入：head = []
//输出：[]
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目在范围 [0, 5 * 10⁴] 内 
// -10⁵ <= Node.val <= 10⁵ 
// 
// Related Topics 链表 双指针 分治 排序 归并排序 👍 1417 👎 0

package leetcode.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

class SortList {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {}

        ListNode(int val) { this.val = val; }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    /**
     * 主要考察3个知识点，
     * 知识点1：归并排序的整体思想
     * 知识点2：找到一个链表的中间节点的方法
     * 知识点3：合并两个已排好序的链表为一个新的有序链表
     *
     * @param args
     */
    public static void main(String[] args) {
        Solution solution = new SortList().new Solution();
        final ListNode listNode = new ListNode(10);
        listNode.next = new ListNode(3);
        listNode.next.next = new ListNode(8);
        listNode.next.next.next = new ListNode(2);
        listNode.next.next.next.next = new ListNode(5);
        listNode.next.next.next.next.next = new ListNode(9);
        listNode.next.next.next.next.next.next = new ListNode(2);
        listNode.next.next.next.next.next.next.next = new ListNode(8);

        ListNode result = solution.sortList(listNode);
        while (result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public ListNode sortList(ListNode head) {
            return head == null ? null :cutLinkedList(head);
        }

        private ListNode cutLinkedList(ListNode head) {
            if (head.next == null) {
                return head;
            }
            //快慢指针
            ListNode pre = null, p1 = head, p2 = head;

            while (p2 != null && p2.next != null) {
                /**
                 * 如果不加这个 head会只有一个节点 不是连续的多个节点,
                 * 这一段的作用是保存前一个节点，即最后跳出循环的中间节点
                 */
                pre = p1;
                p1 = p1.next;
                p2 = p2.next.next;
            }
            pre.next = null;//中间节点，把中间节点的next置空(切断)，即操作head
            ListNode right = p1;

            ListNode l = cutLinkedList(head);
            ListNode r = cutLinkedList(right);
            return mergeNodes(l, r);
        }

        private ListNode mergeNodes(ListNode l1, ListNode l2) {
            final ListNode root = new ListNode(0);
            ListNode cur = root;

            while (l1 != null && l2 != null) {
                if (l1.val > l2.val) {
                    cur.next = l2;
                    l2 = l2.next;
                    cur = cur.next;
                } else {
                    cur.next = l1;
                    l1 = l1.next;
                    cur = cur.next;
                }
            }

            if (l1 != null) {
                cur.next = l1;
            }

            if (l2 != null) {
                cur.next = l2;
            }
            return root.next;
        }

        //ListNode merge(ListNode l, ListNode r) {
        //    ListNode dummyHead = new ListNode(0);
        //    ListNode cur = dummyHead;
        //    while (l != null && r != null) {
        //        if (l.val <= r.val) {
        //            cur.next = l;
        //            cur = cur.next;
        //            l = l.next;
        //        } else {
        //            cur.next = r;
        //            cur = cur.next;
        //            r = r.next;
        //        }
        //    }
        //    if (l != null) {
        //        cur.next = l;
        //    }
        //    if (r != null) {
        //        cur.next = r;
        //    }
        //    return dummyHead.next;
        //}
    }
    //
    //private ListNode mergeSort(ListNode head) {
    //    if (head.next == null) {
    //        return head;
    //    }
    //    ListNode p = head, q = head, pre = null;
    //    while (q != null && q.next != null) {
    //        pre = p;
    //        p = p.next;
    //        q = q.next.next;
    //    }
    //    pre.next = null;
    //    ListNode l = mergeSort(head);
    //    ListNode r = mergeSort(p);
    //    return merge(l, r);
    //}


    //leetcode submit region end(Prohibit modification and deletion)

}
